User delete reassign

Often the need to remove an account from Drupal causes loss of contents because
admins overview the implications of the "Delete the account and its content"
method.

This module tries to solve the problem allowing to disable an account and
assign all of its contents to another one.
