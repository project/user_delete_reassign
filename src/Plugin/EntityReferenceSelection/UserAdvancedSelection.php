<?php

namespace Drupal\user_delete_reassign\Plugin\EntityReferenceSelection;

use Drupal\user\Plugin\EntityReferenceSelection\UserSelection;

/**
 * Provides specific access control for the user entity type.
 *
 * It looks there is a missing feature with UserSelection
 * it is not able to ignore certain users.
 *
 * @EntityReferenceSelection(
 *   id = "default:user_advanced",
 *   label = @Translation("User Advanced selection"),
 *   entity_types = {"user"},
 *   group = "default",
 *   weight = 1
 * )
 */
class UserAdvancedSelection extends UserSelection {

  /**
   * {@inheritdoc}
   */
  protected function buildEntityQuery($match = NULL, $match_operator = 'CONTAINS') {

    $query = parent::buildEntityQuery($match, $match_operator);

    $configuration = $this->getConfiguration();

    // Filter out the selected users if the selection handler is configured to
    // exclude it.
    if (isset($configuration['exclude_users']) && (is_array($configuration['exclude_users'])) && (!empty($configuration['exclude_users']))) {
      $query->condition('uid', $configuration['exclude_users'], 'NOT IN');
      $query->condition('status', 1);
    }

    return $query;

  }

}
